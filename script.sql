DROP TABLE IF EXISTS public.classbe;
CREATE TABLE public.classbe (
	id int NOT NULL,
	nama varchar(50) NOT NULL,
	tema varchar(50) NOT NULL,
	totalpertemuan int NOT NULL,
	CONSTRAINT classbe_pkey PRIMARY KEY (id)
);

DROP TABLE IF EXISTS public.kelas;
CREATE TABLE public.kelas (
	id int NOT NULL,
	id_be int NOT NULL,
	"date" date NOT NULL,
	pertemuanke int NOT NULL,
	CONSTRAINT kelas_pkey PRIMARY KEY (id),
	CONSTRAINT kelas_classbe_fkey FOREIGN KEY (id_be) REFERENCES public.classbe(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO public.classbe VALUES  
	(1, 'zain', 'tipe data', 3),
	(2, 'maya', 'method', 2),
	(3, 'kevin', 'oop inheritance', 3),
	(4, 'luthfi', 'oop encapsulasi', 2),
	(5, 'reyhan', 'oop abrak', 5),
	(6, 'yohana', 'intro postgresql', 1),
	(7, 'indra', 'join', 2),
	(8, 'fahmi', 'spring', 1),
	(9, 'krista', 'spring boot', 7),
	(10, 'zain', 'hibernate', 3)
;

INSERT INTO public.kelas VALUES 
	(1, 1, '2019-10-10', 1),
	(2, 1, '2019-10-09', 2),
	(3, 1, '2019-10-08', 3),
	(4, 2, '2019-07-07', 1),
	(5, 3, '2019-06-06', 1),
	(6, 4, '2019-10-11', 1),
	(7, 4, '2019-10-12', 2),
	(8, 9, '2019-10-13', 1),
	(9, 9, '2019-10-14', 2),
	(10, 9, '2019-10-15', 3)
;

SELECT * FROM public.classbe;

SELECT * FROM public.kelas;

SELECT * FROM public.classbe a
INNER JOIN public.kelas b
ON a.id = b.id_be 
WHERE b."date" > '2019-09-10';

SELECT * FROM public.classbe a 
LEFT JOIN public.kelas b
ON a.id = b.id_be;

SELECT * FROM public.classbe a 
RIGHT JOIN public.kelas b
ON a.id = b.id_be;

SELECT * FROM public.classbe a 
FULL OUTER JOIN public.kelas b
ON a.id = b.id_be;

SELECT * FROM public.classbe a 
WHERE a.tema LIKE 'o___i%';

